# Description précise du projet

## Particularités d'OCaml

### Arguments de fonctions

- Le tilde "~" est utilisé dans les fonctions pour pouvoir déclarer un argument dont le nom est étiquetté, et ainsi avoir une meilleure documentation du code.
- Le point d'interrogation "?" est utilisé dans les fonctions pour déclarer un argument optionnel, dont la valeur par défaut est spécifiée dans la définition de la fonction.

### Modules et foncteurs

- En Ocaml, il est possible de créer des sous-modules avec le mot clef "module". Ils s'utilisent de la même façon que les modules usuels.
- À partir d'un module, il est également possible de faire (dans ce code d'utiliser seulement) des foncteurs, à savoir des molules prenant un argument. On trouve cela dans typing.ml avec Map.make(String), qui définit ainsi un dictionnaire avec des clefs représentées par des chaînes de caractères.
- Dans les foncteurs Map (dictionnaires) et Set (ensembles), le type "t" permet de  définir le type de l'objet. Ainsi, "type t = var M.t" permet de signifier que les éléments du dictionnaire auront un type var (voir le fichier tast.mli).

## Particularités de l'assembleur x86-64

### Utilisation de la pile

- En asssembleur x86-64, la pile s'utilise pour différentes raisons, notamment pour stocker les variables (celles définies autrement que par `new`), ou pour stocker les arguments d'une fonctions en prenant plus que 6.

- Pour l'utiliser, il faut commencer par les instructions `pushq %rbp` et `movq %rsp, %rbp` permettant d'initialiser la pile au registre `%rbp`, puis de conserver l'addresse du sommet de la pile dans `%rsp`. On peut ajouter des éléments sur le sommet de la pile avec `pushq`, et en dépiler pour les placer dans un registre avec `popq [registre]`. Attention : il faut qu'à la fin du programme, tous les éléments empilés soient dépilés pour ne pas provoquer d'erreur de ségmentation. On peut "vider" la pile avec l'instruction `leave` placé avant `ret`.

- On peut accéder à un élément au milieu de la pile en utilisant la syntaxe `nb([registre])` (voir cours programmation 1 sur l'assembleur). Attention : `%rsp` diminue à chaque empilement (le sommet pile est donc en bas).

### Opérande d'addressage

- Pour accéder à la valeur pointée par l'addresse contenue dans un registre `%r`, il faut utiliser la syntaxe `(%r)`.

- Il est possible d'effectuer des opérations simples avec cette opérande : l'opération `nb(%r1, %r2, s)` donne la valeur pointée par l'addresse contenue dans l'addresse `nb + %r1 + s * %r2`. Attention : il est important que `%r1` et `%r2` soient des registres, et que `s` soit 1, 2, 4 ou 8.

### Utilisation de memcpy

La fonction `memcpy` est une fonction de C permettant de copier un ensemble de bits d'une addresse source vers une addresse de destination. Pour l'utiliser en assembleur x86-64, il faut :

- mettre addresse de destination dans le registre `%rdi` (utiliser `leaq` est pertinent dans ce cas) ;

- mettre addresse source dans le registre `%rsi` (utiliser `leaq` est pertinent dans ce cas) ;

- mettre le nombre d'octets à copier dans `%rdx`.

Attention : pour copier des éléments sur la pile, il faut que les addresses de source et de destination pointent la fin des séquences concernées !

## Premier rendu

### Arbres abstraits syntaxiques : ast.mli

Tout ce qui commence par "p" est relatif au parser.

***Parser***
: Analyse syntaxique

- **location** : couple de positions dans un fichier

- **ident** : Couple position/identifiant (usuellement variable ou type)

- **unop** : Opérateurs sur un seul élément
  - **Uneg** : Opposé (nombres)
  - **Unot** : Négation (booléens)
  - **Uamp** : Esperluette (création de pointeurs)
  - **Ustar** : Déférencement de pointeur

- **binop** : Opérateurs sur deux éléments
  - **Badd** / **Bsub** / **Bmul** / **Bdiv** / **Bmod** : Respectivement addition, soustraction, multiplication, quotient et reste d'une division euclidienne
  - **Beq** / **Bne** : Égalité et différence (tous types)
  - **Blt** / **Ble** / **Bgt** / **Bge** : Inégalités (nombres)
  - **Band** / **Bor** : && et ||

- **constant** : Constantes
  - **Cbool** : true / false
  - **Cint** : Entiers signés sur 64 bits
  - **Cstring** : Chaînes de caractères

- **ptyp** : Liste  de ident (nom de types)
  - **PTident** ; Identificateur d'un type
  - **PTptr** : Pointeur vers un autre type

- **incdec** : Incrément et décrement
  - **Inc** : ++
  - **Dec** : --

- **pexpr** : Couple type/position -> Expression

- **pexpr_desc** : Type d'expression
  - **PEskip** : Skip
  - **PEconstant** : Constante
  - **PEbinop** : Opération binaire (opérateur, expression gauche et expression droite)
  - **PEunop** : Opérateur sur un élément (opérateur et expression)
  - **PEnil** : Objet `nil` de GO
  - **PEcall** : Appel d'une fonction (identificateur de la fonction et liste d'arguments)
  - **PEident** : Identificateur d'une variable
  - **PEdot** : Accès au champ d'une structrure (expression s'évaluant en une structure et identificateur du champ)
  - **PEassign** : Assignation de valeurs à des expressions, hors déclaration de variables (liste de l-values et expressions à assigner)
  - **PEvars** : Déclaration de variables, avec ou sans assignation de valeurs (liste d'indentificateurs de variables, type (optionnel), et liste d'expressions à assigner (si nécessaire))
  - **PEif** : Bloc de condition (expression booléenne, liste d'expressions en cas de condition vérifiée et liste d'expressions en cas de condition non vérifiée)
  - **PEreturn** : Déclaration d'un retour de fonction (liste d'expressions retournées)
  - **PEblock** : Bloc d'instructions, utilisé dans les conditions, les boucles, les corps de fonctions, ... (liste d'expressions)
  - **PEfor** : Boucle (expression booléenne et liste d'expressions dans le corps de boucle)
  - **PEincdec** : Opérations d'incrémentation et de décrement (expression et opérateur ++ ou --)

- **pparam** : Couple ident/ptyp -> Paramètre de fonction

- **pfunc** : Ensemble de nom/paramètres/ptyp/expression -> Fonction

- **pfield** : Couple ident/ptyp -> Champ d'une structure (nom et liste de types)

- **pstruct** : Ensemble de nom/champs -> Structure

- **pdecl** : Déclaration d'une fonction ou d'une structure

- **import_fmt** : Booléen (Est-ce que le package "fmt" a été importé ?)

- **pfile** : Ensemble de import_fmt et des déclarations d'un fichier

### Arbres abstraits syntaxiques typés : tast.mli

Tout ce qui commence par "t" est relatif au typer.

***Typer***
: Typage des expressions parsées

- **unop**/**binop**/**constant**/**incdec** : Identiques à ceux dans ast.mli

- **function** : Ensemble d'un nom, d'une liste de variables, d'une liste  de types de retour et d'une localisation

- **structure**: Ensemble d'un nom, d'un ensemble de champs stockées dans une table de hachage et d'une locatlisation

- **typ** : Types
  - **Tvoid** : Type void
  - **Tint** : Entier signé
  - **Tbool** : Booléen
  - **Tstring** : Chîne de caractères
  - **Tptr** : Pointeur vers un autre type
  - **Tstruct** : Structures définies par l'utilisateur
  - **Tmany** : n-uplet de types

- **var** : Ensmeble d'un nom, d'un identifiant entier unique, d'une localisation, d'un type, d'un booléen pour savoir si la variable est utilisée, et d'un booléen pou savoir si cette variable possède un pointeur

- **field** : Ensemble d'un nom, d'un type, et d'un entier relatif à l'addresse de l'objet

- **expr** : Expression composée d'une description et d'un type

- **expr_desc** : Description d'une expression
  - **TEskip** : Skip
  - **TEconstant** : Constante
  - **TEbinop** : Identique à PEBinop
  - **TEunop** : Identique à PEunop
  - **TEnil** : Identique à PEnil
  - **TEnew** : Appel de la fonction `new` servant à l'allocation mémoire (type de variable)
  - **TEcall** : Appel d'une fonction définie par l'utilisateur (fonction et liste d'expressions correspondant aux arguments)
  - **TEident** : Variable
  - **TEdot :** Accès au champ d'une structure (expression s'évaluant en une structure, et champ de la structure)
  - **TEassign** : Assignations d'expressions à des l-values (liste de l-values et liste d'expressions)
  - **TEvars :** Déclaration de variables (liste de variables)
  - **TEif :** Bloc conditionnel (expression s'évaluant en booléen, bloc si vrai et bloc si faux)
  - **TEreturn** : Déclaration d'un retour de fonction (liste d'expressions de retour)
  - **TEblock** : Bloc d'instructions, utilisé dans les conditions, les boucles, ... (liste d'expressions)
  - **TEfor :** Boucle (expression s'évaluant en une condition et corps de boucle sous la forme d'une expression (TEblock))
  - **TEprint** : Appel à la fonction `print` (liste d'expressions à afficher)
  - **TEincdec** : Opération d'incrémentation ou de décrement (expression à incrémenter/décrémenter et opérateur ++ ou --)

- **tdecl** : Déclaration
  - **TDfunction** : Déclaration d'une fonction (fonction et liste d'expressions à appliquer à la fonction (arguments))
  - **TDstruct** : Déclaration d'une structure

- **tfile** : Fichier (ensemble de déclarations)

### Affichage des AST et des TAST : pretty.ml

Les affichages codés en début de projet ont été abandonnés pour utiliser à la place un affichage dans un fichier .svg (image vectorielle) généré à chaque compilation. Les codes ayant permi cela ont été écrit par Vincent Lafeychine.

### Typage : typing.ml

Ce fichier est le fichier principal à modifier pour le permier rendu. Pour cela, il a fallu effectuer 3 étapes :

1. Ajout des structures au contexte pour connaître leur existence et vérifier qu'il n'y a pas de doublons.

2.
    1. Ajout des fonctions au contexte en vérifiant l'unicité du nom des fonctions, le type et l'unicité des arguments.
    2. Fin d'ajout des structures au contexte en vérifiant que les champs sont distincts et bien formés.

3.
    1. Fin d'ajout des fonctions au contexte en ajoutant les variables au contexte, en vérifiant leur utilisation, et en vérifiant également les contraintes liées à `return`.
    2. On vérifie par ailleurs que les structures ne sont pas récursives.

- **debug** : Booléen indiquant si le débugage de ce fichier est actif

- **dummy_loc** : Position impossible utilisée pour les erreurs sans position précise

- *error*  loc e : Fonction renvoyant une erreur e (string) à la position loc

- **Structure** : Sous-module des structures
  - **Struct** : Création du dictionnaire stockant les structures par leur nom
  - **t**/*empty_env_struct*/*find*/*mem*/*update* : Raccourcis d'accès au type et aux méthodes de **Struct**
  - *add_strutcture* env strct : Ajoute la structure strct à l'environnement env

- **Function** : Sous-module des fonctions (types et méthodes identiques à **Structure**)

- **env_struct** : Environnement des structures (utilisant le sous-module **Structure**)

- **env_func** : Environnement des structures (utilisant le sous-module **Function**)

- **actual_func_type** : Liste de types utilisée pour vérifier les types des instructions `return` dans une fonction

- **block_counter** : Compteur global de TEblock

- **block_phaser** : Référence indiquant le bloc actuel

- **actual_blocks_vars** : Liste de couples (entier, nom) recensant l'ensemble des variables de chaque bloc (grâce à **bloc_counter** et **block_phaser**) pour ne pas pouvoir redéfinir des variables déjà existantes

- *type_type* typ loc : Convertit le **Ast.ptype** typ en un **Tast.typ**

- *test_type* typ loc : Teste l'existence du **Tast.typ** typ

- *eq_type* ty1 ty2 : Teste l'égalité entre les **Tast.typ** ty1 et ty2

- *is_ptr* typ : Teste si le **Tast.typ** typ est un pointeur

- **fmt_used** : Booléen indiquant si le module "fmt" a été utilisé

- **fmt_imported** : Booléen indiquant si le module "fmt" a été importé

- *evar* v : Transforme la variable v en une expression

- *new_var* x loc ?(used=false) ty : Crée une nouvelle variable nommée x, de position loc, de type ty (used indique si elle a été utilisée) en lui donnant un unique identifiant

- **Env** : Sous-module des variables
  - **M**/**t**/*empty*/*find*/*add* : Identique à **Structure** et **Function**
  - **all_vars** : Liste de toutes les variables
  - **check_unused** : Vérifie que toutes les variables ont été utilisé et renvoie une erreur sinon
  - **var** : Crée une variable de la même manière que **new_var** en l'ajoutant à *all_vars*
  
- *make* d ty : Crée une expression à partir de la description d et du type ty

- *stmt* d : Crée une expression de description d et de type void

- *test_lvalue* e_desc : Teste si la description d'une expression est une l-value ou non

- *expr* env e : Évalue la p-expression e dans l'environnement env et renvoie une nouvelle t-expression ainsi qu'un booléen indiquant s'il y a une instruction `return` dans l'expression évaluée

- **found_main** : Booléen indiquant si la fonction main est bien définie

- *somme_liste* l : Renvoie la somme de la liste d'entiers l

- *sizeof* ty : Renvoie le nombre de bits nécessaires à réserver dans la mémoire pour stocker une variable de type ty

- *hashtabl_to_list* : Fonction prenant une table de hachage en entrée et renvoyant une liste de couples (clef, valeur) présentes dans la table

## Second rendu
