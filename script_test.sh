#!/usr/bin/env bash

# void executeCommand(String msg, ...cmd)
function executeCommand()
{
    echo -ne "$1: "
    shift

    if ! OUTPUT=$("$@" 2>&1); then

        tput setaf 1; echo -n "KO"; tput setaf 7

        echo ""
        echo "${OUTPUT}"

        exit 1

    fi

    tput setaf 2; echo -n "OK"; tput setaf 7
    echo ""
}

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 [prenom_nom.tgz]"
    exit 1
fi


FILENAME=$(basename -- "$1")
EXTENSION="${FILENAME#*.}"
FOLDER="${FILENAME%%.*}"

[[ ${EXTENSION} =~ ^(tar.gz|tgz)$ ]] && COND=true || COND=false
executeCommand "Archive contenant la bonne extension" ${COND}

TMP_DIR=$(mktemp -d) || exit 1

executeCommand "Décompression de l'archive $1" tar xzf "$1" -C "${TMP_DIR}"

find "${TMP_DIR}" -maxdepth 2 -type f -name "*.md" -o -name "*.txt" -o -name "*.pdf" | grep -q . && COND=true || COND=false
executeCommand "Présence d'un rapport dans l'archive" ${COND}

[[ -d "${TMP_DIR}/${FOLDER}" ]] && COND=true || COND=false
executeCommand "Présence du dossier ${FOLDER} dans l'archive" ${COND}

[[ -f "${TMP_DIR}/${FOLDER}/Makefile" ]] && COND=true || COND=false
executeCommand "Présence du fichier Makefile dans le dossier ${FOLDER}" ${COND}

[[ -f "${TMP_DIR}/${FOLDER}/pgoc" ]] && COND=false || COND=true
executeCommand "Non-présence du binaire pgoc dans le dossier ${FOLDER}" ${COND}

executeCommand "Compilation du projet afin d'obtenir le binaire pgoc" make -C "${TMP_DIR}/${FOLDER}"

executeCommand "Éxécution du binaire pgoc" "${TMP_DIR}/${FOLDER}/pgoc" --help

rm -rf "${TMP_DIR}"