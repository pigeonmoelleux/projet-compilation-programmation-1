(* étiquettes
     F_function      entrée fonction
     E_function      sortie fonction
     L_xxx           sauts
     S_xxx           chaîne

   expression calculée avec la pile si besoin, résultat final dans %rdi

   fonction : arguments sur la pile, résultat dans %rax ou sur la pile

            res k
            ...
            res 1
            arg n
            ...
            arg 1
            adr. retour
   rbp ---> ancien rbp
            ...
            var locales
            ...
            calculs
   rsp ---> ...

*)

open Format
open Ast
open Tast
open X86_64
open Errors

let debug = ref false

let strings = Hashtbl.create 32
let alloc_string =
  let r = ref 0 in
  fun s ->
    incr r;
    let l = "S_" ^ string_of_int !r in
    Hashtbl.add strings l s;
    l

let malloc n = movq (imm n) (reg rdi) ++ call "malloc"
let allocz n = movq (imm n) (reg rdi) ++ call "allocz"

let sizeof = Typing.sizeof
let make = Typing.make

let label_counter = ref 0 
let new_label () = incr label_counter; "L_" ^ string_of_int !label_counter

let block_counter = ref (-1)
let block_phaser = ref (-1)
let blocks_father = ref [||]

let actual_function = ref {fn_name = ""; fn_params = []; fn_typ = []; fn_loc = Typing.dummy_loc}

let is_struct = fun e -> match e.expr_typ with | Tstruct _ -> true | _ -> false
let is_ptr_struct = fun e -> match e.expr_typ with | Tptr Tstruct _ -> true | _ -> false

module Portee = struct
    type t = string * int
    let compare = compare
end

module Vars_env = struct
    module V = Map.Make(Portee)
    type t = Tast.var V.t
    
    let counter = ref 0
    
    let empty = V.empty
    let mem name env =
        let actual_block = ref !block_phaser and found = ref false in
        while !actual_block <> -1  && not !found do
            if V.mem (name, !actual_block) env then found := true else actual_block := !blocks_father.(!actual_block)
        done;
        !found

    let find name env = 
        let actual_block = ref !block_phaser and var = ref { v_name = ""; v_id = -1; v_loc = Typing.dummy_loc; v_typ = Tvoid; v_used = false; v_addr = false; stack_position = 0 } in
        while !actual_block <> -1  && !var.v_name = "" do
            if V.mem (name, !actual_block) env then var := V.find (name, !actual_block) env else actual_block := !blocks_father.(!actual_block)
        done;
        !var

    let add env ?(take_counter : int option) {v_name; v_id; v_loc; v_typ; v_used; v_addr} = match take_counter with
        | None -> let new_env = V.add (v_name, !block_phaser) {v_name = v_name; v_loc = v_loc; v_id = v_id; v_used = v_used; v_typ = v_typ; v_addr = v_addr ; stack_position = !counter} env
            in counter := !counter + sizeof v_typ / 8; new_env
        | Some c -> V.add (v_name, !block_phaser) {v_name = v_name; v_loc = v_loc; v_id = v_id; v_used = v_used; v_typ = v_typ; v_addr = v_addr ; stack_position = c} env

    let variables_in_block i env =
        let var_list = ref [] in
        V.iter (fun (_, j) var -> if i = j then var_list := var::!var_list) env;
        !var_list

    let print : Tast.var V.t -> unit = V.iter (fun (name, block) {stack_position} -> eprintf "%s -> %d : %d\n" name stack_position block)
end

let range n =
    let rec aux n = match n with
        | 0 -> [0]
        | _ when n > 0 -> n::(aux (n-1))
        | _ -> []
    in List.rev (aux (n - 1))

let env_vars :  Tast.var Vars_env.V.t ref = ref Vars_env.empty

let mk_bool d = { expr_desc = d; expr_typ = Tbool }

let rec get_addr left_value = match left_value.expr_desc with
    | TEident {v_name} ->
        let position = (let x = Vars_env.find v_name !env_vars in x.stack_position) in
        movq (imm (- position)) !%r10 ++
        leaq (ind ?ofs:(Some 0) ?index:(Some r10) ?scale:(Some 8) rbp) r13
    | TEunop (Ustar, lv) ->
        expr lv ++
        popq r13
    | TEdot (e, {f_ofs}) when is_struct e -> 
        get_addr e ++
        leaq (ind ?ofs:(Some (- f_ofs)) r13) r13
    | TEdot (e, {f_ofs}) when is_ptr_struct e -> 
        get_addr e ++
        movq (ind r13) !%r13 ++
        leaq (ind ?ofs:(Some (- f_ofs)) r13) r13
    | _ -> assert false

and end_assign left_value = match left_value with
    | {expr_desc=TEident {v_name; v_typ = Tstruct s}} ->
        let position = (let v = Vars_env.find v_name !env_vars in v.stack_position) in
        movq (imm (- position)) !%r10 ++
        leaq (ind ?ofs:(Some (- sizeof (Tstruct s) + 8)) ?index:(Some r10) ?scale:(Some 8) rbp) rdi ++
        leaq (ind ?ofs:None ?index:None ?scale:None rsp) rsi ++
        movq (imm (sizeof (Tstruct s))) !%rdx ++
        call "memcpy"
    | {expr_desc=TEident x} -> 
        let position = (let v = Vars_env.find x.v_name !env_vars in v.stack_position) in
        popq rdi ++
        movq (imm (- position)) !%r10 ++
        movq !%rdi (ind ?ofs:(Some 0) ?index:(Some r10) ?scale:(Some 8) rbp)
    | {expr_desc = TEunop (Ustar, lv)} ->
        expr lv ++
        popq rbx ++
        popq rdi ++
        movq !%rdi (ind ?ofs:None ?index:None ?scale:None rbx)
    | {expr_desc = TEdot ({expr_desc; expr_typ = Tstruct s} as struc, {f_name; f_typ; f_ofs=ofs})} ->
        get_addr struc ++
        movq !%r13 !%rdi ++
        subq (imm ofs) !%rdi ++
        leaq (ind ?ofs:None ?index:None ?scale:None rsp) rsi ++
        movq (imm (sizeof f_typ)) !%rdx ++
        call "memcpy" ++
        addq (imm (sizeof f_typ)) !%rsp
    | {expr_desc = TEdot ({expr_desc; expr_typ = Tptr Tstruct s} as struc, {f_name; f_typ; f_ofs=ofs})} ->
        get_addr struc ++
        movq (ind ?ofs:None ?index:None ?scale:None r13) !%rdi ++
        subq (imm ofs) !%rdi ++
        leaq (ind ?ofs:None ?index:None ?scale:None rsp) rsi ++
        movq (imm (sizeof f_typ)) !%rdx ++
        call "memcpy" ++
        addq (imm (sizeof f_typ)) !%rsp
    | _ -> assert false

and expr e =
    match e.expr_desc with
    | TEskip ->
        nop
    | TEconstant (Cbool true) ->
        pushq (imm 1)
    | TEconstant (Cbool false) ->
        pushq (imm 0)
    | TEconstant (Cint x) ->
        pushq (imm64 x)
    | TEnil ->
        xorq (reg rdi) (reg rdi) ++
        pushq !%rdi
    | TEconstant (Cstring s) ->
        let label = alloc_string s in
        movq  (ilab label) (reg rdi) ++
        pushq !%rdi
    | TEbinop (Band | Bor as op, e1, e2) ->
        let l_lazy = new_label () and end_label = new_label () in
        expr e1 ++
        popq rdi ++
        testq !%rdi !%rdi ++
        (match op with 
            | Band -> jz l_lazy
            | Bor -> jne l_lazy
            | _ -> nop) ++
        expr e2 ++
        jmp end_label ++
        label l_lazy ++
        pushq !%rdi ++
        label end_label
    | TEbinop (Blt | Ble | Bgt | Bge as op, e1, e2) ->
        let success_label = new_label () and fail_label = new_label () in
        expr e2 ++
        expr e1 ++
        popq rdi ++
        movq !%rdi !%rsi ++
        popq rdi ++
        subq !%rdi !%rsi ++
        (match op with
            | Blt -> jl
            | Ble -> jle
            | Bgt -> jg
            | Bge -> jge
            | _ -> assert false) success_label ++
        movq (imm 0) !%rdi ++
        jmp fail_label ++
        label success_label ++
        movq (imm 1) !%rdi ++
        label fail_label ++
        pushq !%rdi
    | TEbinop (Badd | Bsub | Bmul | Bdiv | Bmod as op, e1, e2) ->
        expr e1 ++
        expr e2 ++
        popq rdi ++
        movq !%rdi !%rsi ++
        popq rdi ++
        (match op with
            | Badd -> addq !%rsi !%rdi
            | Bsub -> subq !%rsi !%rdi
            | Bmul -> imulq !%rsi !%rdi
            | Bdiv | Bmod as op -> 
                movq !%rdi !%rax ++
                xorq !%rdx !%rdx ++
                idivq !%rsi ++
                (match op with
                    | Bdiv -> movq !%rax !%rdi
                    | Bmod -> movq !%rdx !%rdi
                    | _ -> nop)
            | _-> nop) ++
        pushq !%rdi
    | TEbinop (Beq | Bne as op, e1, e2) ->
        let success_label = new_label () and fail_label = new_label () in
        begin
            match e1.expr_typ with
                | Tstruct _-> assert false
                | Tstring -> 
                    expr e2 ++
                    expr e1 ++
                    popq rdi ++
                    popq rsi ++
                    call "strcmp" ++
                    pushq !%rax 
                | Tvoid | Tmany _ -> assert false
                | Tptr _ | Tint | Tbool ->
                    expr e2 ++
                    popq rdi ++
                    movq !%rdi !%rsi ++
                    expr e1 ++
                    popq rdi ++
                    cmpq !%rsi !%rdi ++
                    (match op with
                        | Beq -> jz
                        | Bne -> jnz
                        | _ -> assert false) success_label ++
                    movq (imm 0) !%rdi ++
                    jmp fail_label ++
                    label success_label ++
                    movq (imm 1) !%rdi ++
                    label fail_label
        end ++
        pushq !%rdi
    | TEunop (Uneg, e1) ->
        expr e1 ++
        popq rdi ++
        negq !%rdi ++
        pushq !%rdi
    | TEunop (Unot, e1) ->
        let f_label = new_label () and end_label = new_label () in
        expr e1 ++
        popq rdi ++
        testq !%rdi !%rdi ++
        jz f_label ++
        movq (imm 0) !%rdi ++
        jmp end_label ++
        label f_label ++
        movq (imm 1) !%rdi ++
        label end_label ++
        pushq !%rdi
    | TEunop (Uamp, {expr_desc = TEident x; expr_typ = typ}) ->
        let position = (let v = Vars_env.find x.v_name !env_vars in v.stack_position) in
        movq !%rbp !%rbx ++
        movq (imm 8) !%r15 ++
        imulq (imm (- position)) !%r15 ++
        addq !%r15 !%rbx ++
        movq !%rbx !%rdi ++
        pushq !%rdi
    | TEunop (Uamp, {expr_desc = TEunop (Ustar, lv); expr_typ = typ}) ->
        expr lv
    | TEunop (Uamp, {expr_desc = TEdot (e, field); expr_typ = typ}) when is_struct e ->
        get_addr e ++
        subq (imm field.f_ofs) !%r13 ++
        pushq !%r13
    | TEunop (Uamp, {expr_desc = TEdot (e, field); expr_typ = typ}) when is_ptr_struct e ->
        get_addr e ++
        movq (ind r13) !%r13 ++
        subq (imm field.f_ofs) !%r13 ++
        pushq !%r13
    | TEunop (Uamp, _) ->
        assert false
    | TEunop (Ustar, ({expr_desc; expr_typ = Tptr Tstruct s})) ->
        assert false
    | TEunop (Ustar, e1) ->
        expr e1 ++
        popq rdi ++
        movq (ind ?ofs:None ?index:None ?scale:None rdi) !%r15 ++
        movq !%r15 !%rdi ++
        pushq !%rdi
    | TEprint el ->
        let rec call_print typ = match typ with
            | Tvoid -> assert false
            | Tint -> call "print_int"
            | Tbool -> call "print_bool"
            | Tstring -> call "print_string"
            | Tptr Tvoid -> call "print_nil"
            | Tptr _ -> call "print_int_hex"
            | Tstruct s -> assert false
            | Tmany _ -> assert false in
        let expr_array = Array.of_list (el@[make TEskip Tvoid]) in
        List.fold_left (++) nop (
            List.map2 (fun e i ->
                expr e ++
                let rec print_typ typ = match typ with
                    | Tptr (Tstruct s) ->
                        let skip_label = new_label () and end_label = new_label () in
                        let ordered_fields = List.sort (fun (_, f1) (_, f2) -> f1.f_ofs - f2.f_ofs) (Typing.hashtbl_to_list s.s_fields) in
                        expr e ++
                        popq rdi ++
                        testq !%rdi !%rdi ++
                        jz skip_label ++
                        call "print_ampersand" ++
                        call "print_Lbracket" ++
                        List.fold_left (fun asm (_, field) ->
                            asm ++
                            expr e ++
                            popq rdi ++
                            pushq (ind ?ofs:(Some (-field.f_ofs)) ?index:None ?scale:None rdi) ++
                            if is_ptr_struct (make TEskip field.f_typ) then print_typ (Tptr Tint) else print_typ field.f_typ ++
                            if (fst (List.hd (List.rev ordered_fields)) <> field.f_name) then call "print_space" else nop) nop ordered_fields ++
                        call "print_Rbracket" ++
                        jmp end_label ++
                        label skip_label ++
                        call "print_nil" ++
                        addq (imm 8) !%rsp ++
                        label end_label
                    | Tint | Tbool | Tptr _ ->
                        popq rdi ++
                        call_print typ ++
                        (if expr_array.(i+1).expr_typ <> Tstring && expr_array.(i+1).expr_typ <> Tvoid then call "print_space" else nop)
                    | Tstring ->
                        popq rdi ++
                        call_print typ
                    | Tstruct s ->
                        let ordered_fields = List.sort (fun (_, f1) (_, f2) -> f1.f_ofs - f2.f_ofs) (Typing.hashtbl_to_list s.s_fields) in
                        call "print_Lbracket" ++
                        List.fold_left (fun asm (_, field) ->
                            asm ++
                            expr (make (TEdot (e, field)) field.f_typ) ++
                            if is_ptr_struct (make TEskip field.f_typ) then print_typ (Tptr Tint) else print_typ field.f_typ ++
                            if (fst (List.hd (List.rev ordered_fields)) <> field.f_name) then call "print_space" else nop) nop ordered_fields ++
                        call "print_Rbracket"
                    | Tmany type_list -> 
                        let size = sizeof typ / 8 in
                        let rec aux type_list i = match type_list with
                            | [] -> nop
                            | ty::other_types -> 
                                movq (imm (size - i)) !%r10 ++
                                movq (ind ?ofs:(Some 0) ?index:(Some r10) ?scale:(Some 8) rsp) !%rdi ++
                                call_print ty ++
                                (if List.length other_types <> 0 then call "print_space" else nop) ++
                                aux other_types (i + (sizeof ty) / 8)
                        in aux type_list 1
                    | Tvoid -> assert false
                in print_typ e.expr_typ
            ) el (range (List.length el))
        )
    | TEident {v_name; v_typ = Tstruct s} -> 
        let v = Vars_env.find v_name !env_vars in
        subq (imm (sizeof v.v_typ)) !%rsp ++
        leaq (ind ?ofs:(Some (- sizeof (Tstruct s) - v.stack_position * 8 + 8)) ?index:(None) ?scale:(None) rbp) rsi ++
        leaq (ind ?ofs:(Some 0) ?index:(None) ?scale:(None) rsp) rdi ++
        movq (imm (sizeof v.v_typ)) !%rdx ++
        call "memcpy"
    | TEident x ->
        let v = Vars_env.find x.v_name !env_vars in
        movq (imm (- v.stack_position)) !%r10 ++
        movq (ind ?ofs:(Some 0) ?index:(Some r10) ?scale:(Some 8) rbp) !%rdi ++
        pushq !%rdi
    | TEassign ([left_expr], [right_expr]) ->
        expr right_expr ++
        end_assign left_expr
    | TEassign (left_values, [{expr_desc=TEcall (f, el); expr_typ} as right_value]) ->
        expr right_value ++
        List.fold_right (fun lv asm -> asm ++ end_assign lv) left_values nop ++
        addq (imm (sizeof expr_typ)) !%rsp
    | TEassign (left_values, exprs) ->
        List.fold_left (fun asm e -> asm ++ expr e) nop exprs ++
        List.fold_right (fun lv asm -> asm ++ end_assign lv) left_values nop
    | TEblock el ->
        incr block_counter;
        let previous_block = !block_phaser and previous_counter = !Vars_env.counter in
        block_phaser := !block_counter;
        blocks_father := Array.append !blocks_father [|previous_block|];
        let asm = List.fold_left (++) nop (List.map (expr) el) in
        let asm' = asm ++ addq (imm (List.fold_left (+) 0 (List.map (fun var -> sizeof var.v_typ) (Vars_env.variables_in_block !block_phaser !env_vars)))) !%rsp in
        block_phaser := previous_block;
        Vars_env.counter := previous_counter;
        asm'
    | TEif (e1, e2, e3) ->
        let f_cond = new_label () and after = new_label () in
        expr e1 ++
        popq rdi ++
        testq !%rdi !%rdi ++
        jz f_cond ++
        expr e2 ++
        jmp after ++
        label f_cond ++
        expr e3 ++
        label after
    | TEfor (e1, e2) ->
        let ending = new_label () and evaluation = new_label () in
        label evaluation ++
        expr e1 ++
        popq rdi ++
        testq !%rdi !%rdi ++
        jz ending ++
        expr e2 ++
        jmp evaluation ++
        label ending
    | TEnew ty ->
        let rec put_zeros i = match i with
            | 0 -> nop
            | _ -> movq (imm 0) (ind ?ofs:(Some i) ?index:None ?scale:None rbx) ++ put_zeros (i + 8) in
        movq (imm (sizeof ty)) !%rdi ++
        call "malloc" ++
        movq !%rax !%rbx ++
        put_zeros (- sizeof ty) ++
        pushq !%rax
    | TEcall (f, el) ->
        if List.length f.fn_typ = 0 then
            List.fold_right (fun arg asm -> asm ++ expr arg) (List.rev el) nop ++
            call ("F_" ^ f.fn_name) 
        else if sizeof (Tmany f.fn_typ) / 8 = 1 then
            begin
                List.fold_right (fun arg asm -> asm ++ expr arg) (List.rev el) nop ++
                call ("F_" ^ f.fn_name) ++
                addq (imm (sizeof (Tmany (List.map (fun var -> var.v_typ) f.fn_params)))) !%rsp ++
                pushq !%rax
            end
        else
            begin
                subq (imm (sizeof (Tmany f.fn_typ))) !%rsp ++
                List.fold_right (fun arg asm -> asm ++ expr arg) (List.rev el) nop ++
                call ("F_" ^ f.fn_name) ++
                addq (imm (sizeof (Tmany (List.map (fun var -> var.v_typ) f.fn_params)))) !%rsp
            end
    | TEdot (e1, {f_typ; f_ofs=ofs}) when is_struct e1 && not (is_struct (make TEskip f_typ)) ->
        let s = match e1.expr_typ with
            | Tstruct s -> s
            | _ -> assert false in
        expr e1 ++
        movq (ind ?ofs:(Some (sizeof (Tstruct s) - ofs - 8)) ?index:None ?scale:None rsp) !%rdi ++
        addq (imm (sizeof (Tstruct s))) !%rsp ++
        pushq !%rdi
    | TEdot (e1, {f_typ; f_ofs=ofs}) when is_ptr_struct e1 && not (is_struct (make TEskip f_typ)) ->
        expr e1 ++
        popq rdi ++
        pushq (ind ?ofs:(Some (-ofs)) ?index:None ?scale:None rdi)
    | TEdot _ -> assert false
    | TEvars ([], []) -> assert false
    | TEvars (vars, [{expr_desc=TEcall (f, el); expr_typ}]) when List.length vars > 1 ->
        let asm = expr (make (TEvars (vars, [])) (Tvoid)) in
        asm ++ expr (make (TEassign (List.map (fun var -> make (TEident var) var.v_typ) vars, [make (TEcall (f, el)) expr_typ])) (Tvoid)) ++
        subq (imm (sizeof expr_typ)) !%rsp
    | TEvars (vars, []) when List.fold_left (&&) true (List.map (fun var -> is_struct (make (TEident var) var.v_typ)) vars) -> 
        List.fold_left (fun asm var ->
            let s = match var.v_typ with 
                | Tstruct s -> s
                | _ -> assert false in
            env_vars := Vars_env.add !env_vars ?take_counter:None var;
            asm ++
            let rec pre_assign structure =
                let ordered_fields = List.sort (fun (_, f1) (_, f2) -> f1.f_ofs - f2.f_ofs) (Typing.hashtbl_to_list structure.s_fields) in
                List.fold_left (fun asm (_,{f_typ; f_ofs}) -> asm ++ match f_typ with
                    | Tint -> expr (make (TEconstant (Cint Int64.zero)) Tint)
                    | Tstring -> expr (make (TEconstant (Cstring "")) Tstring)
                    | Tbool -> expr (make (TEconstant (Cbool false)) Tbool)
                    | Tptr _ -> expr (make TEnil (Tptr var.v_typ))
                    | Tstruct s' -> pre_assign s'
                    | _ -> assert false) nop ordered_fields
                in pre_assign s
            ) nop vars
    | TEvars (vars,[]) ->
        expr (Typing.make (TEvars (vars, List.map (fun var -> Typing.make (match var.v_typ with
            | Tint -> TEconstant (Cint Int64.zero)
            | Tstring -> TEconstant (Cstring "")
            | Tbool -> TEconstant (Cbool false)
            | Tptr _ -> TEnil
            | _ -> assert false) var.v_typ) vars)) Tvoid)
    | TEvars (vars, texprs) ->
        List.fold_left2 (fun asm x e -> 
            let asm' =
                asm ++
                (match e.expr_typ with
                    | Tint | Tstring | Tbool | Tptr _ ->
                        expr e
                    | Tstruct s ->
                        subq (imm (sizeof (Tstruct s))) !%rsp
                    | _ -> assert false)
            in env_vars := Vars_env.add !env_vars ?take_counter:None x; asm') nop vars texprs
    | TEreturn [] ->
        xorq !%rax !%rax ++
        jmp ("E_" ^ !actual_function.fn_name)
    | TEreturn [e1] ->
        expr e1 ++
        popq rdi ++
        movq !%rdi !%rax ++
        jmp ("E_" ^ !actual_function.fn_name)
    | TEreturn expr_list ->
        let rec aux exprs i = match exprs with
            | [] -> nop
            | e::end_expr_list -> 
                expr e ++
                popq rdi ++
                movq (imm i) !%r10 ++
                movq !%rdi (ind ?ofs:(Some 0) ?index:(Some r10) ?scale:(Some 8) rbp) ++
                aux end_expr_list (i - 1)
        in aux expr_list (List.length !actual_function.fn_typ + List.length !actual_function.fn_params + 1)
    | TEincdec (e1, op) ->
        expr (Typing.make (TEassign ([e1],[Typing.make (TEbinop (Badd, e1, Typing.make (TEconstant (Cint (Int64.of_int (match op with
            | Inc -> 1
            | Dec -> -1
        )))) Tint)) Tint])) Tvoid)

let function_ f e =
    if !debug then eprintf "function %s:@." f.fn_name;
    let s = f.fn_name in
    actual_function := f;
    Vars_env.counter := 1;
    let previous_counter = !block_phaser in
    block_phaser := !block_counter + 1;
    (if List.length f.fn_params <> 0 then 
        let rec aux params i = match params with
            | [] -> ()
            | var::other_params -> env_vars := Vars_env.add !env_vars var ?take_counter:(Some i); aux other_params (i + sizeof var.v_typ / 8)
    in aux f.fn_params (- (sizeof (Tmany (List.map (fun var -> var.v_typ) f.fn_params))) / 8 - 1));
    block_phaser := previous_counter;
    let asm = 
    label ("F_" ^ s) ++
    pushq !%rbp ++
    movq !%rsp !%rbp ++
    expr e ++
    label ("E_" ^ s) ++
    movq !%rbp !%rsp ++
    popq rbp ++
    ret in
    asm

let decl code = function
    | TDfunction (f, e) -> code ++ function_ f e
    | TDstruct s -> if !debug then eprintf "structure %s@." s.s_name; code

let file ?debug:(b=true) dl =
    debug := b;
    (* TODO calcul offset champs *)
    (* TODO code fonctions *) 
    let funs = List.fold_left decl nop dl in
    if !debug then Vars_env.print !env_vars;
    { text =
        globl "main" ++ label "main" ++
        call "F_main" ++
        xorq (reg rax) (reg rax) ++
        ret ++
        funs ++
        inline "

print_int:
    movq    %rdi, %rsi
    movq    $S_int, %rdi
    xorq    %rax, %rax
    call    printf
    ret

print_int_hex:
    movq    %rdi, %rsi
    testq   %rdi, %rdi
    jz      print_nil
    movq    $S_int_hex, %rdi
    xorq    %rax, %rax
    call    printf
    ret

print_string:
    mov     %rdi, %rsi
    mov     $S_string, %rdi
    xorq    %rax, %rax
    call    printf
    ret

print_Lbracket:
    mov     %rdi, %rsi
    mov     $S_Lbracket, %rdi
    xorq    %rax, %rax
    call    printf
    ret

print_Rbracket:
    mov     %rdi, %rsi
    mov     $S_Rbracket, %rdi
    xorq    %rax, %rax
    call    printf
    ret

print_nil:
    mov     $S_nil, %rdi
    xorq    %rax, %rax
    call    printf
    ret

print_space:
    mov     $S_space, %rdi
    xorq    %rax, %rax
    call    printf
    ret

print_bool:
    xorq    %rax, %rax
    test    %rdi, %rdi
    jz      print_bool_false
    mov     $S_true, %rdi
    call    printf
    ret

print_bool_false:
    mov     $S_false, %rdi
    xorq    %rax, %rax
    call printf
    ret

print_ampersand:
    movq $S_ampersand, %rdi
    xorq    %rax, %rax
    call printf
    ret

print_backspace:
    movq $S_backspace, %rdi
    xorq    %rax, %rax
    call printf
    ret

allocz:
    movq %rdi, %rbx     # callee-save
    call malloc
    testq %rbx, %rbx
    jnz 1f
    ret
1:      
    movb $0, (%rax, %rbx)
    decq %rbx
    jnz 1b
    ret

    ";
    
    data =
        label "S_int" ++ string "%ld" ++
        label "S_int_hex" ++ string "0x%x" ++
        label "S_string" ++ string "%s" ++
        label "S_true" ++ string "true" ++
        label "S_false" ++ string "false" ++
        label "S_nil" ++ string "<nil>" ++
        label "S_space" ++ string " " ++
        label "S_return" ++ string "\n" ++
        label "S_empty" ++ string "" ++
        label "S_ampersand" ++ string "&" ++
        label "S_Lbracket" ++ string "{" ++
        label "S_Rbracket" ++ string "}" ++
        label "S_backspace" ++ string "\b" ++
        (Hashtbl.fold (fun l s d -> label l ++ string s ++ d) strings nop)
    ;
    }
