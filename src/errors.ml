open Lexing
open Tast

let ast_location ({pos_fname=pos_fname1; pos_lnum=pos_lnum1; pos_bol=pos_bol1; pos_cnum=_}, {pos_fname=_; pos_lnum=pos_lnum2; pos_bol=pos_bol2; pos_cnum=_}) = 
    String.concat " " ["line"; string_of_int pos_lnum1; "char"; string_of_int pos_bol1;]

let rec tast_type typ : string = match typ with (* Convertit un Tast.typ en string pour l'afficher *)
    | Tvoid -> "void"
    | Tint -> "int"
    | Tbool -> "bool"
    | Tstring -> "string"
    | Tstruct s -> "struct " ^ s.s_name
    | Tptr ptr -> "pointer " ^ (tast_type ptr)
    | Tmany type_list -> String.concat " " (("[")::(List.map (fun ty -> tast_type ty) type_list)@["]"])

let type_error typ expected_typ : string = (* Renvoie un string à utiliser dans les erreurs de types (lorsque le type obtenu n'est pas celui attendu) *)
    "This expression has type " ^ tast_type typ ^ " but " ^ tast_type expected_typ ^ " was expected"