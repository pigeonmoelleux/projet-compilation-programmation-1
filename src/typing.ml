open Format
open Lib
open Ast
open Tast
open Errors

let debug = ref true

let dummy_loc = Lexing.dummy_pos, Lexing.dummy_pos

exception Error of Ast.location * string

let error loc e = raise (Error (loc, e))

module Structure = struct (* Sous-module utilisé pour les structures *)
    module Struct = Map.Make(String)
    type t = tdecl Struct.t

    let empty_env_structure = Struct.empty
    let find = fun name env -> 
        let ds = Struct.find name env in match ds with
            | TDstruct s -> s
            | TDfunction (f,_) -> error f.fn_loc ("The function" ^ f.fn_name ^ "is declared as a structure")

    let mem = Struct.mem
    
    let add_structure env strct = 
        if not (mem strct.s_name env) 
            then 
                begin
                    Struct.add strct.s_name (TDstruct strct) env
                end
            else 
                begin
                    error strct.s_loc (String.concat " " 
                    ["The structure"; strct.s_name; "has already been defined at"; 
                    let s = Struct.find strct.s_name env in match s with
                        | TDstruct s -> Errors.ast_location s.s_loc
                        | _ -> error dummy_loc "This case is technically impossible"
                    ])
                end

    let update = Struct.update
end

module Function = struct (* Sous-module utilisé pour les fonctions *)
    module Fun = Map.Make(String)
    type t = tdecl Fun.t

    let empty_env_function = Fun.empty
    
    let find = fun name env -> 
        let ds = Fun.find name env in match ds with
            | TDfunction (f,_) -> f
            | TDstruct s -> error s.s_loc ("The structure " ^ s.s_name ^ " is declared as a function")
    
    let mem = Fun.mem
    
    let add_function env (func,expr) = 
        if not (Fun.mem func.fn_name env) 
            then Fun.add func.fn_name (TDfunction (func,expr)) env 
            else error func.fn_loc (String.concat " " 
                ["The function"; func.fn_name; "has already been defined at"; 
                let f = Fun.find func.fn_name env in match f with
                    | TDfunction (f,_) -> Errors.ast_location f.fn_loc
                    | _ -> error dummy_loc "This case is technically impossible"
                ])
    let update = Fun.update
end

let env_struct :  Tast.tdecl Structure.Struct.t ref = ref Structure.empty_env_structure (* Environnement des structures *)
let env_func : Tast.tdecl Function.Fun.t ref = ref Function.empty_env_function (* Environnement des fonctions *)
let actual_func_type : Tast.typ list ref = ref [Tvoid] (* Variable utile pour vérifier le typage des instructions return *)

let block_counter = ref 0 (* Compteur global de bloc pour leur donner un identifiant unique *)
let block_phaser = ref 0 (* Variable indiquant dans quel bloc se situe chaque déclaration de variable *)
let actual_blocks_vars : (int * string) list ref = ref [] (* Variable utile pour vérifier que chaque variable est définie de manière unique dans chaque bloc *)

let rec type_type = function
    | PTident { id = "int" } -> Tint
    | PTident { id = "bool" } -> Tbool
    | PTident { id = "string" } -> Tstring
    | PTptr ty -> Tptr (type_type ty)
    | PTident { id = id } when Structure.mem id !env_struct -> Tstruct(Structure.find id !env_struct) 
    | PTident { id = id; loc = loc } -> error loc ("Unknown structure " ^ id)

let test_type typ loc : unit = (* Cette fonction envoie une erreur si le type typ existe, et ne fait rien sinon *)
    try let _ = type_type typ in () with Not_found -> error loc "This type doesn't exist"

let rec eq_type ty1 ty2 = match ty1, ty2 with
    | Tint, Tint | Tbool, Tbool | Tstring, Tstring | Tvoid, Tvoid -> true
    | Tstruct s1, Tstruct s2 -> s1.s_name == s2.s_name
    | Tptr Tvoid, Tptr _ | Tptr _, Tptr Tvoid -> true (* Cette condition permet d'affecter la valeur nil a un pointeur bien défini *)
    | Tptr ty1, Tptr ty2 -> eq_type ty1 ty2
    | Tmany l1, Tmany l2 -> 
        let rec aux p1 p2 = match p1,p2 with
            | [], [] -> true
            | t1::q1, t2::q2 -> (eq_type t1 t2) && aux q1 q2
            | _ -> false
        in aux l1 l2
    | _ -> false

let is_ptr typ : bool = match typ with (* Renvoie si le type est un pointeur *)
    | Tptr _ -> true
    | _ -> false

let is_many typ : bool = match typ with (* Renvoie si le type est un Tmany *)
    | Tmany _ -> true
    | _ -> false

let fmt_used = ref false
let fmt_imported = ref false

let evar v = { expr_desc = TEident v; expr_typ = v.v_typ }

let new_var =
  let id = ref 0 in
  fun x loc ?(used=false) ty ->
    incr id;
    { v_name = x; v_id = !id; v_loc = loc; v_typ = ty; v_used = used; v_addr = false; stack_position = 0 }

module Env = struct
    module M = Map.Make(String)
    type t = var M.t
    let empty = M.empty
    let find = M.find
    let add env v = M.add v.v_name v env

    let all_vars = ref []
    let check_unused () =
        let check v =
            if v.v_name <> "_" && not v.v_used then error v.v_loc ("unused variable " ^ v.v_name) in
        List.iter check !all_vars

    let var x loc ?used ty env =
        let v = new_var x loc ?used ty in
        all_vars := v :: !all_vars;
        add env v, v
end

let make d ty = { expr_desc = d; expr_typ = ty }
let stmt d = make d Tvoid

let compress_type type_list : typ = match type_list with (* Convertit les listes de types en un unique type (éventuellement Tmany) *)
    | [] | [Tvoid] -> Tvoid
    | [typ] -> typ
    | _ -> Tmany type_list

let rec test_lvalue e_desc : bool = match e_desc with (* Teste si l'expr_desc est une l-value ou non *)
    (* On suppose dans cette fonction que l'expression e est bien typée *)
    | TEdot (e,_) -> test_lvalue e.expr_desc
    | TEunop(Ustar, e) -> (not (eq_type e.expr_typ Tvoid)) || (is_ptr e.expr_typ)
    | TEvars _ | TEident _ -> true
    | _ -> false

let rec expr env e =
    let e, ty, rt = expr_desc env e.pexpr_loc e.pexpr_desc in
        { expr_desc = e; expr_typ = ty }, rt

and expr_desc (env : Tast.var Env.M.t ref) loc = function
    | PEskip ->
        TEskip, Tvoid, false
    
    | PEconstant c -> 
        let ty = match c with
            | Cbool _ -> Tbool
            | Cint _ -> Tint
            | Cstring _ -> Tstring
        in TEconstant c, ty, false
    
    | PEbinop (op, e1, e2) ->
        let e1, _ = expr env e1 and e2, _ = expr env e2 in
        let e = TEbinop(op, e1, e2) in 
        let ty = match op with
            | Badd | Bsub | Bmul | Bdiv | Bmod -> 
                if e1.expr_typ == Tint && e2.expr_typ == Tint then Tint else error loc (Errors.type_error (if e1.expr_typ <> Tint then e1.expr_typ else e2.expr_typ) Tint)
            | Beq | Bne -> if eq_type e1.expr_typ e2.expr_typ then Tbool else error loc (Errors.type_error e2.expr_typ e1.expr_typ)
            | Blt | Ble | Bgt | Bge -> 
                if e1.expr_typ == Tint && e2.expr_typ == Tint then Tbool else error loc (Errors.type_error (if e1.expr_typ <> Tint then e1.expr_typ else e2.expr_typ) Tint)
            | Band | Bor -> 
                if e1.expr_typ == Tbool && e2.expr_typ == Tbool then Tbool else error loc (Errors.type_error (if e1.expr_typ <> Tbool then e1.expr_typ else e2.expr_typ) Tbool)
        in e, ty, false
    
    | PEunop (Uamp, e1) ->
        let e2, _ = expr env e1 in
        let e = TEunop(Uamp, e2) in 
        if not (test_lvalue e2.expr_desc) then error loc "This expression cannot be the target of a pointer";
        let typ = Tptr e2.expr_typ in
        e, typ, false
    
    | PEunop (Uneg | Unot | Ustar as op, e1) ->
        let e2, _ = expr env e1 in
        let e = TEunop(op, e2) in 
        let typ = match op with
            | Uneg -> if e2.expr_typ == Tint then Tint else error loc (Errors.type_error e2.expr_typ Tint)
            | Unot -> if e2.expr_typ == Tbool then Tbool else error loc (Errors.type_error e2.expr_typ Tbool)
            | Ustar -> 
                let ptr_type = match e2.expr_typ with
                    | Tptr Tvoid -> error loc "It is impossible to deference a nil pointer"
                    | Tptr ty -> ty
                    | _ -> error loc ("This expression has type " ^ (Errors.tast_type e2.expr_typ) ^ " but a pointer was expected")
                in ptr_type
            | _ -> error loc "This cas is technically impossible" (* Ce cas est mis ici pour éviter les warnings *)
        in e, typ, false
    
    | PEcall ({id = "fmt.Print"}, el) ->
        let exprs = List.map (fun x -> let e,_ = expr env x in e) el in
        fmt_used := true;
        let type_list = List.map (fun expr -> expr.expr_typ) exprs in
        if List.mem Tvoid type_list then error loc "You cannot print a void expression";
        if (List.fold_left (||) false (List.map is_many type_list)) && List.length exprs > 1 then error loc "You cannot print a multiple-value expression";
        TEprint exprs, Tvoid, false
    
    | PEcall ({id="new"}, [{pexpr_desc=PEident {id}}]) ->
        let ty = match id with
            | "int" -> Tint | "bool" -> Tbool | "string" -> Tstring | "void" -> Tvoid
            | name when Structure.mem name !env_struct -> Tstruct(Structure.find name !env_struct)
            | _ -> error loc ("no such type " ^ id) in
        TEnew ty, Tptr ty, false
    
    | PEcall ({id="new"}, _) ->
        error loc "new expects a type"
    
    | PEcall (id, el) when Function.mem id.id !env_func -> (* Cas où l'on appelle une autre fonction *)
        let f = Function.find id.id !env_func in
        let texpr_list, _ = List.split (List.map (expr env) el) in
        if not (eq_type (compress_type (List.map (fun expr -> expr.expr_typ) texpr_list)) (compress_type (List.map (fun var -> var.v_typ) f.fn_params))) then error loc ("The function " ^ f.fn_name ^ " was called with " ^ (Errors.tast_type (compress_type (List.map (fun expr -> expr.expr_typ) texpr_list))) ^ " argument(s) but " ^ (Errors.tast_type (compress_type (List.map (fun var -> var.v_typ) f.fn_params))) ^ " was/were expected"); 
        TEcall (f, texpr_list), compress_type f.fn_typ, false
    
    | PEcall (id, el) -> error id.loc ("The function " ^ id.id ^ " doesn't exist")
    
    | PEfor (e, b) ->
        let e', _ = expr env e and b', _ = expr env b in
        if eq_type e'.expr_typ Tbool then TEfor (e', b'), Tvoid, false else error loc (Errors.type_error e'.expr_typ Tbool)
    
    | PEif (e1, e2, e3) ->
        let e, _ = expr env e1 and b1, rt1 = expr env e2 and b2, rt2 = expr env e3 in
        if eq_type e.expr_typ Tbool 
            then TEif (e, b1, b2), Tvoid, rt1 && rt2 
            else error loc (Errors.type_error e.expr_typ Tbool)
    
    | PEnil ->
        TEnil, Tptr Tvoid, false (* On donne à nil le type void* *)
    
    | PEident {id=id} ->
        if id = "_" then error loc "The variable _ cannot be used as a value";
        (try let v = Env.find id !env in v.v_used <- true; TEident v, v.v_typ, false
        with Not_found -> error loc ("unbound variable " ^ id))
    
    | PEdot (e, id) ->
        let e', _ = expr env e in
        let _ = match e'.expr_typ with
            | Tstruct _ | Tptr (Tstruct _) -> ()
            | _ -> error loc (Errors.type_error e'.expr_typ (Tstruct { s_name = ""; s_fields = Hashtbl.create 5; s_loc = dummy_loc})) in
        (try
            let s = Structure.find (
                match e'.expr_typ with
                    | Tstruct s -> s.s_name
                    | Tptr (Tstruct s) -> s.s_name
                    | _ -> error loc "This case is technically impossible" (* Ce cas est impossible grâce au test précédent *)
            ) !env_struct in
            if Hashtbl.mem s.s_fields id.id then () else error loc ("The field " ^ id.id ^ " doesn't exist");
            let field = Hashtbl.find s.s_fields id.id in
            TEdot (e', field), field.f_typ, false
        with 
            Not_found -> error loc ("The structure " ^ (Errors.tast_type e'.expr_typ) ^ " doesn't exist"))
    
    | PEassign (lvl, el) ->
        let is_used = List.map (fun lv -> 
            try
                let v = match lv.pexpr_desc with
                    | PEident ({id; loc})  -> if id = "_" then (let new_env, v = Env.var "_" loc ?used:(Some true) Tvoid !env in env := new_env; v) else Env.find id !env
                    | _ -> error loc "Pass" 
                in v.v_used
            with _ -> false) lvl in
        let vars, _ = List.split (List.map 
            (fun e ->
                try expr env e
                with 
                    | Error (_, "The variable _ cannot be used as a value") -> let v = Env.find "_" !env in make (TEident v) Tvoid, false
                    | Error (l, msg) -> error l msg
                )lvl) in
        if not (List.fold_left (fun bool e -> bool && (test_lvalue e.expr_desc)) true vars) then error loc "This expression cannot be assigned";
        List.iter2 (fun var used -> 
            try let v = match var.expr_desc with
                | TEident (v) -> v
                | _ -> error loc "Pass"
            in v.v_used <- used
        with _ -> ()) vars is_used;
        let  values, _ = List.split (List.map (expr env) el) in
        let value_types = match compress_type (List.map (fun value -> value.expr_typ) values) with
            | Tmany tl -> tl
            | ty -> [ty]
        in if List.length vars <> List.length value_types then error loc ("You cannot assigned " ^ string_of_int (List.length value_types) ^ " value(s) to " ^ string_of_int (List.length vars) ^ " var(s)");
        if List.fold_left (&&) true (List.map2 
            (fun var value_type -> 
                try 
                    let eq_underscore = match var.expr_desc with
                        | TEident (v) -> v.v_name = "_"
                        | _ -> false 
                    in eq_underscore || eq_type var.expr_typ value_type
                with _ -> eq_type var.expr_typ value_type) vars value_types)
            then TEassign (vars, values), Tvoid, false
            else error loc (Errors.type_error (compress_type (List.map (fun var -> var.expr_typ) vars)) (compress_type (List.map (fun value -> value.expr_typ) values)) )
    
    | PEreturn el ->
        let e', _ = List.split (List.map (expr env) el) in 
        (try
            if not (List.fold_left (&&) true (List.map2 (fun e typ -> eq_type e.expr_typ typ) e' !actual_func_type)) then error loc (Errors.type_error (compress_type (List.map (fun e -> e.expr_typ) e')) (compress_type !actual_func_type));
            TEreturn e', Tvoid, true
        with _ -> error loc "This return statement has the wrong number of arguments")
    
    | PEblock el ->
        let local_env = ref !env in
        incr block_counter;
        let block_father = !block_phaser in
        block_phaser := !block_counter;
        let e', rt = List.split (List.map (expr local_env) el) in
        block_phaser := block_father;
        TEblock e', Tvoid, (List.fold_left (||) false rt)
    
    | PEincdec (e, op) ->
        let e', _ = expr env e in
        if eq_type e'.expr_typ Tint 
            then 
                (if not (test_lvalue e'.expr_desc) then error loc "This expression cannot be assigned";
                TEincdec (e', op), Tvoid, false) 
            else error loc (Errors.type_error e'.expr_typ Tint)
    
    | PEvars (vars,typ,pexprs) ->
        let rec is_there_void t : bool = match t with
            | Tvoid -> true
            | Tmany tl -> List.fold_left (||) false (List.map is_there_void tl)
            | _ -> false
        in let texprs, _ = List.split (List.map (expr env) pexprs) in
        let tevars =
        (List.iter (fun {id;_} -> if not (List.mem (!block_phaser,id) !actual_blocks_vars) then (if id <> "_" then actual_blocks_vars := (!block_phaser,id)::!actual_blocks_vars) else error loc ("The variable " ^ id ^ " has already been defined in this block")) vars;
        match typ with
            | None -> 
                if List.length texprs == 0 then error loc "Type of the expression undefined";
                let types = compress_type (List.map (fun e -> e.expr_typ) texprs) in
                let type_list = match types with
                    | Tmany tl -> tl
                    | _ -> [types]
                in if List.length vars == List.length type_list 
                    then
                        (let rec is_there_nil t : bool = match t with
                            | Tptr Tvoid -> true
                            | Tptr ptr -> is_there_nil ptr
                            | Tmany tl -> List.fold_left (||) false (List.map is_there_nil tl)
                            | _ -> false in
                        if List.fold_left (||) false (List.map is_there_nil type_list)then error loc "Use of untyped nil";
                        if List.fold_left (||) false (List.map is_there_void type_list)then error loc "Cannot assign a void expression";
                        List.iter2 (fun var typ -> env := Env.add !env (new_var var.id var.loc typ)) vars type_list;
                        List.map2 (fun var typ -> let new_env, v = Env.var var.id var.loc typ !env in env := new_env; v) vars type_list)
                    else error loc ("You cannot assigned " ^ string_of_int (List.length type_list) ^ " value(s) to " ^ string_of_int (List.length vars) ^ " var(s)")
            | Some typ ->
                let _ = test_type typ loc in
                match texprs with
                    | [] -> List.iter (fun var -> env := Env.add !env (new_var var.id var.loc (type_type typ))) vars; 
                            List.map (fun var -> let new_env, v = Env.var var.id var.loc (type_type typ) !env in env := new_env; v) vars
                    | _ -> let types = compress_type (List.map (fun e -> e.expr_typ) texprs) in
                            let type_list = match types with
                                | Tmany tl -> tl
                                | _ -> [types]
                            in if List.length type_list == List.length vars
                                then if (List.fold_left (&&) true (List.map (fun t -> eq_type t (type_type typ)) type_list))
                                    then
                                        (if List.fold_left (||) false (List.map is_there_void type_list)then error loc "Cannot assign a void expression";
                                        List.iter (fun var -> env := Env.add !env (new_var var.id var.loc (type_type typ))) vars;
                                        List.map2 (fun var typ -> let new_env, v = Env.var var.id var.loc typ !env in env := new_env; v) vars type_list)
                                    else error loc (Errors.type_error (compress_type type_list) (type_type typ))
                                else error loc ("You cannot assigned " ^ string_of_int (List.length type_list) ^ " value(s) to " ^ string_of_int (List.length vars) ^ " var(s)"))
            in TEvars (tevars, texprs), Tvoid, false

let found_main = ref false

(* 1. declare structures *)
let phase1 : pdecl -> unit = function
    | PDstruct { ps_name = {id;loc}; } -> 
        if List.mem id ["int"; "string"; "bool"] then error loc ("You cannot override the definition of " ^ id); 
        env_struct := Structure.add_structure !env_struct {s_name = id; s_fields = Hashtbl.create 5; s_loc = loc};
    | PDfunction _ -> ()

let rec somme_list l : int = match l with (* Fait la somme d'une liste d'entiers *)
    | [] -> 0
    | x::p -> x + somme_list p

let rec sizeof = function
    | Tvoid | Tint | Tbool | Tstring | Tptr _ -> 8
    | Tmany l -> 8 * (List.length l)
    | Tstruct s -> 
        let sum = Hashtbl.fold (fun name field sum -> sum + sizeof field.f_typ) s.s_fields 0 in
        sum + if sum mod 8 == 0 then 0 else (8 - (sum mod 8))

(* 2. declare functions and type fields *)
let phase2 : pdecl -> unit = function
    | PDfunction f ->
        let param_to_var =
            let id = ref 0 in
            fun x loc ty ->
            { v_name = x; v_id = !id; v_loc = loc; v_typ = ty; v_used = false; v_addr = false; stack_position = 0 } in

        let arg_names = ref [] in
        List.iter (fun (ident, _) -> (* On vérifie ici que les noms des variables sont distincts deux à deux *)
            if not (List.mem ident.id !arg_names) then arg_names := ident.id::!arg_names 
            else error ident.loc ("The name " ^ ident.id ^ " has already been declared as an other argument.")) f.pf_params;
        
        (* On vérifie ici que les types des variables existent *)
        List.iter (fun (ident,typ) -> test_type typ ident.loc) f.pf_params;
        
        let params = List.map (fun (ident, ptyp) -> param_to_var ident.id ident.loc (type_type ptyp)) f.pf_params in
        let fn_typ = List.map type_type f.pf_typ in
        env_func := Function.add_function !env_func ({fn_name = f.pf_name.id; fn_params = params; fn_typ = if (compress_type fn_typ) = Tvoid then [Tvoid] else fn_typ; fn_loc = f.pf_name.loc}, {expr_desc = TEskip; expr_typ = Tvoid});

    | PDstruct { ps_name = {id; loc}; ps_fields = field_list } ->
        let compteur = ref 0 in
        let pfield_to_tfield (ident, ptyp) = 
            let t_field = ident.id, {f_name = ident.id; f_typ = type_type ptyp; f_ofs = !compteur} in
            compteur := !compteur + 8;
            t_field in

        let field_names = ref [] in
        List.iter (fun (ident, _) -> (* On vérifie ici que les noms des variables sont distincts deux à deux *)
            if not (List.mem ident.id !field_names) then field_names := ident.id::!field_names 
            else error ident.loc ("The name " ^ ident.id ^ " has already been declared as an other field.")) field_list;
        
        List.iter (fun (ident,typ) -> test_type typ ident.loc) field_list;
        
        let add_field s = match s with
            | Some (TDstruct {s_name = s_name; s_fields = s_fields; s_loc = s_loc}) -> 
                List.iter (fun (ident, ptyp) -> Hashtbl.add s_fields ident.id (let _, fld = pfield_to_tfield (ident, ptyp) in fld)) field_list;
                Some (TDstruct {s_name = s_name; s_fields = s_fields; s_loc = s_loc})
            | _ -> None in
        env_struct := Structure.update id add_field !env_struct

(* 3. type check function bodies *)
let hashtbl_to_list : ('a, 'b)  Hashtbl.t -> ('a * 'b) list = fun hash -> Hashtbl.fold (fun key value list -> (key, value)::list) hash []

let decl = function
    | PDfunction { pf_name = {id; loc}; pf_body = e; pf_typ = tyl; pf_params = pf_params} ->
        let {fn_name; fn_params; fn_typ; fn_loc} = Function.find id !env_func
        in let env = ref Env.empty in
        if fn_name = "main" then 
            begin
                found_main := true;
                if (compress_type fn_typ) <> Tvoid then error loc ("The main function has type " ^ (Errors.tast_type (compress_type fn_typ)) ^ " but void was expected");
                if List.length fn_params > 0 then error loc "The main function has at least one argument but 0 was expected";
            end;
        actual_func_type := fn_typ;
        List.iter (fun (ident,ptyp) -> env := let new_env, _ = Env.var ident.id ident.loc ?used:(Some true) (type_type ptyp) !env in new_env) pf_params;
        let e, rt = expr env e in
        if not rt && not (eq_type (compress_type fn_typ) Tvoid) then error fn_loc ("One of the branch of the function " ^ fn_name ^ " does not lead to a return statement");
        TDfunction ({fn_name = fn_name; fn_typ = fn_typ; fn_loc = fn_loc; fn_params = fn_params}, e)

    | PDstruct {ps_name={id; loc}} ->
        let s = Structure.find id !env_struct in
        let rec find_recursion strc structs_seen = 
                List.iter (fun (_,field) -> if List.fold_left (||) false (List.map (fun strct -> eq_type field.f_typ (Tstruct strct)) structs_seen) then error s.s_loc ("The structure " ^ s.s_name ^ " is recursive"); 
                match field.f_typ with 
                    | Tstruct strc' -> find_recursion strc' (strc::structs_seen)
                    | _ -> ())
                (hashtbl_to_list strc.s_fields);
        in find_recursion s [s];
        TDstruct s

let file ~debug:b (imp, dl) =
    debug := b;
    fmt_imported := imp;
    List.iter phase1 dl;
    List.iter phase2 dl;
    let dl = List.map decl dl in
    if not !found_main then error dummy_loc "missing method main";
    Env.check_unused ();
    if imp && not !fmt_used then error dummy_loc "fmt imported but not used";
    if not imp && !fmt_used then error dummy_loc "fmt not imported but used";
    dl