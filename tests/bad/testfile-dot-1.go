package main

import "fmt"

type T struct {
	a int
}

func main() {
	var b = 3
	fmt.Print(b.a)
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-dot-1.go", line 11, characters 11-14:
error: This expression has type int but struct  was expected
*/
