package main

import "fmt"

func test(a int) *int {
	return &a
}

func void_func() {
	fmt.Print("Bonjour")
}

func main() {
	var b = test(3)
	*b = 1
	var c *int = &(void_func())
	b = c
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-pointer-1.go", line 16, characters 14-28:
error: This expression cannot be the target of a pointer
*/
