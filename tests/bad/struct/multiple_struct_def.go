package main

type A struct {
	x int
}

type A struct {
	x int
}

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/struct/multiple_struct_def.go", line 7, characters 5-6:
error: The structure A has already been defined at line 3 char 14
*/
