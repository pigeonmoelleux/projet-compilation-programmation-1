package main

type A struct {
	b B
}

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/struct/unknown_field_struct.go", line 4, characters 3-4:
error: Unknown structure B
*/
