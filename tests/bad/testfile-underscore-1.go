package main

import "fmt"

func main() {
	var _ = 3
	fmt.Print(_)
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-underscore-1.go", line 7, characters 11-12:
error: The variable _ cannot be used as a value
*/
