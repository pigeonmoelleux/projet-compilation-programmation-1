package main

func main() {
	var a, b int

	a, b = 1, 2, 3
}

/*
== Expected compiler output ==
File "./../tests/bad/assign/bad_unpack.go", line 6, characters 1-15:
error: You cannot assigned 3 value(s) to 2 var(s)
*/
