package main

func main() {
	var x int

	x = _
}

/*
== Expected compiler output ==
File "./../tests/bad/assign/underscore.go", line 6, characters 5-6:
error: The variable _ cannot be used as a value
*/
