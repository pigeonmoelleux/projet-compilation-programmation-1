package main

func foo() (int, string) {
	return 1, "Hello"
}

func main() {
	var a, b int

	a, b = foo()
}

/*
== Expected compiler output ==
File "./../tests/bad/assign/incompatible_types_func.go", line 10, characters 1-13:
error: This expression has type [ int int ] but [ int string ] was expected
*/
