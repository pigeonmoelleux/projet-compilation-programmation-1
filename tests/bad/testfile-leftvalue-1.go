package main

func main() { 1 = 2 }

/*
== Expected compiler output ==
File "./../tests/bad/testfile-leftvalue-1.go", line 3, characters 14-19:
error: This expression cannot be assigned
*/
