package main

import "fmt"

func test() bool {
	var a = 1
	if a > 3 {
		return false
	}
}

func main() {
	fmt.Print("Bonjour")
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-return-1.go", line 5, characters 5-9:
error: One of the branch of the function test does not lead to a return statement
*/
