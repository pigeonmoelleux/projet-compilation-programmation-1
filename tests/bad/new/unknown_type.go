package main

func main() {
	var a = new(L)
}

/*
== Expected compiler output ==
File "./../tests/bad/new/unknown_type.go", line 4, characters 9-15:
error: no such type L
*/
