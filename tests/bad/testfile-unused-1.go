package main

func main() { x := 1 }

/*
== Expected compiler output ==
File "./../tests/bad/testfile-unused-1.go", line 3, characters 14-15:
error: unused variable x
*/
