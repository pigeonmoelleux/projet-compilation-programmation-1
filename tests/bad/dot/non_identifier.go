package main

func main() {
	2.x = 10
}

/*
== Expected compiler output ==
File "./../tests/bad/dot/non_identifier.go", line 4, characters 1-4:
error: This expression has type int but struct  was expected
*/
