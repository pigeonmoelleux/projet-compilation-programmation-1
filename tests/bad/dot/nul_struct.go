package main

func main() {
	nil.x = 10
}

/*
== Expected compiler output ==
File "./../tests/bad/dot/nul_struct.go", line 4, characters 1-6:
error: This expression has type pointer void but struct  was expected
*/
