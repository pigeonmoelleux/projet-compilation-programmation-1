package main

func foo(a int) int {
	return a
}

func main() {
	foo(1, 2)
}

/*
== Expected compiler output ==
File "./../tests/bad/call/mismatch_params_number.go", line 8, characters 1-10:
error: The function foo was called with [ int int ] argument(s) but int was/were expected
*/
