package main

func foo(a int) int {
	return a
}

func main() {
	foo("Hello")
}

/*
== Expected compiler output ==
File "./../tests/bad/call/mismatch_param_type.go", line 8, characters 1-13:
error: The function foo was called with string argument(s) but int was/were expected
*/
