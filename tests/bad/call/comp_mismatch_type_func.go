package main

func foo() (int, string) {
	return 12, "Hello"
}

func bar(a int, b int) int {
	return a + b
}

func main() {
	bar(foo())
}

/*
== Expected compiler output ==
File "./../tests/bad/call/comp_mismatch_type_func.go", line 12, characters 1-11:
error: The function bar was called with [ int string ] argument(s) but [ int int ] was/were expected
*/
