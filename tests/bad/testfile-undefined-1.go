package main

type T struct {
	a int
}

func main() {
	var s S
	s.a = 0
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-undefined-1.go", line 8, characters 7-8:
error: Unknown structure S
*/
