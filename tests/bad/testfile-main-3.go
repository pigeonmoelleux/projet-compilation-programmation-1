package main

import "fmt"

func main() int {
	fmt.Print("Hello")
	return 1
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-main-3.go", line 5, characters 5-9:
error: The main function has type int but void was expected
*/
