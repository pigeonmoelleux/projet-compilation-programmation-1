package main

type A struct {
	b B
}

type B struct {
	c C
}

type C struct {
	b B
}

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-struct-recursion-2.go", line 3, characters 5-6:
error: The structure A is recursive
*/
