package main

import "fmt"

func main() {
	var x, y = nil, 1
	fmt.Print(x, y)
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-nil-1.go", line 6, characters 1-18:
error: Use of untyped nil
*/
