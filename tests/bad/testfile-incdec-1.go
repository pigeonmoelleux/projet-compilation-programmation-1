package main

func main() {
	1++
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-incdec-1.go", line 4, characters 1-4:
error: This expression cannot be assigned
*/
