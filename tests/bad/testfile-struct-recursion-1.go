package main

import "fmt"

type a struct {
	t b
}

type b struct {
	t a
}

func main() {
	fmt.Print("Ok")
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-struct-recursion-1.go", line 5, characters 5-6:
error: The structure a is recursive
*/
