package main

func foo() (int, int) {
	return 42, "Hello"
}

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/return/bad_return_type.go", line 4, characters 1-19:
error: This return statement has the wrong number of arguments
*/
