package main

func f() int {}

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/return/no_return.go", line 3, characters 5-6:
error: One of the branch of the function f does not lead to a return statement
*/
