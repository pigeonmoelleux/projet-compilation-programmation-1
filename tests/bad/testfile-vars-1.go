package main

func main() {
	var a = 1
	a = a + 1
	var a = 2
	a = a + 1
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-vars-1.go", line 6, characters 1-10:
error: The variable a has already been defined in this block
*/
