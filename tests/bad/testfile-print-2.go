package main

import "fmt"

func foo() (int, int) { return 1, 2 }

func main() {
	fmt.Print(foo(), "\n")
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-print-2.go", line 8, characters 1-23:
error: You cannot print a multiple-value expression
*/
