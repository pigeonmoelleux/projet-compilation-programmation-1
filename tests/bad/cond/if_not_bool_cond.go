package main

func main() {
	if 42 {

	}
}

/*
== Expected compiler output ==
File "./../tests/bad/cond/if_not_bool_cond.go", line 4, characters 1-12:
error: This expression has type int but bool was expected
*/
