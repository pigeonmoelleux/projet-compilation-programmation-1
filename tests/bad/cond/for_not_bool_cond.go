package main

func main() {
	for i := 1; 42; i++ {
		fmt.Print(i)
	}
}

/*
== Expected compiler output ==
File "./../tests/bad/cond/for_not_bool_cond.go", line 4, characters 1-40:
error: This expression has type int but bool was expected
*/
