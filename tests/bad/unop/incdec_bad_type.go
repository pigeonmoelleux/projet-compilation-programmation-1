package main

func main() {
	"Hello"++
}

/*
== Expected compiler output ==
File "./../tests/bad/unop/incdec_bad_type.go", line 4, characters 7-10:
error: This expression has type string but int was expected
*/
