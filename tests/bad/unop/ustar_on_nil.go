package main

func main() {
	*nil
}

/*
== Expected compiler output ==
File "./../tests/bad/unop/ustar_on_nil.go", line 4, characters 1-5:
error: It is impossible to deference a nil pointer
*/
