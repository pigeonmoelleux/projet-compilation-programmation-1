package main

func main() {
	!"Hello"
}

/*
== Expected compiler output ==
File "./../tests/bad/unop/unot_bad_type.go", line 4, characters 1-9:
error: This expression has type string but bool was expected
*/
