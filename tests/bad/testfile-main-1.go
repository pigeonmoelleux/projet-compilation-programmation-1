package main

func foo() int { return 42 }

/*
== Expected compiler output ==
File "./../tests/bad/testfile-main-1.go", line 0, characters -1--1:
error: missing method main
*/
