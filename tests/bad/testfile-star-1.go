package main

import "fmt"

func main() {
	var a *int = nil
	fmt.Print(*a)
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-star-1.go", line 7, characters 11-13:
error: It is impossible to deference a nil pointer
*/
