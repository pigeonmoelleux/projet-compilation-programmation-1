package main

func main() {
	fmt.Print("Hello World")
}

/*
== Expected compiler output ==
File "./../tests/bad/fmt/unimported_fmt.go", line 0, characters -1--1:
error: fmt not imported but used
*/
