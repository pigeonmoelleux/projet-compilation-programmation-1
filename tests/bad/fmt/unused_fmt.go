package main

import "fmt"

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/fmt/unused_fmt.go", line 0, characters -1--1:
error: fmt imported but not used
*/
