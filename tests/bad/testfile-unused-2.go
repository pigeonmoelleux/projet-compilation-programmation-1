package main

func main() {
	var a = 1
	a = 2
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-unused-2.go", line 4, characters 5-6:
error: unused variable a
*/
