package main

type int struct{}

func main() {
	var a int = 3
	a = a + 1
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-overwrite-1.go", line 3, characters 5-8:
error: You cannot override the definition of int
*/
