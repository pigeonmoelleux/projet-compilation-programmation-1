package main

import "fmt"

func foo() {}

func main() {
	fmt.Print(foo())
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-print-1.go", line 8, characters 1-17:
error: You cannot print a void expression
*/
