package main

func main(i int) {}

/*
== Expected compiler output ==
File "./../tests/bad/main/parameters_main.go", line 3, characters 5-9:
error: The main function has at least one argument but 0 was expected
*/
