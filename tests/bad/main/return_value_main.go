package main

func main() int {
	return 42
}

/*
== Expected compiler output ==
File "./../tests/bad/main/return_value_main.go", line 3, characters 5-9:
error: The main function has type int but void was expected
*/
