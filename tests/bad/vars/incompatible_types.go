package main

func main() {
	var a int = "Hello world"
}

/*
== Expected compiler output ==
File "./../tests/bad/vars/incompatible_types.go", line 4, characters 1-26:
error: This expression has type string but int was expected
*/
