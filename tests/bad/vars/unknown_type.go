package main

func main() {
	var a A
}

/*
== Expected compiler output ==
File "./../tests/bad/vars/unknown_type.go", line 4, characters 7-8:
error: Unknown structure A
*/
