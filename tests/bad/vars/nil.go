package main

func main() {
	var x = nil
}

/*
== Expected compiler output ==
File "./../tests/bad/vars/nil.go", line 4, characters 1-12:
error: Use of untyped nil
*/
