package main

import "fmt"

func main(a int) {
	fmt.Print("Hello")
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-main-2.go", line 5, characters 5-9:
error: The main function has at least one argument but 0 was expected
*/
