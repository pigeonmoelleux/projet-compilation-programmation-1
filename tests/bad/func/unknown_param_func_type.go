package main

func f(a A) {}

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/func/unknown_param_func_type.go", line 3, characters 9-10:
error: Unknown structure A
*/
