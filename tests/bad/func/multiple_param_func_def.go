package main

func f(a int, a int) {}

func main() {}

/*
== Expected compiler output ==
File "./../tests/bad/func/multiple_param_func_def.go", line 3, characters 14-15:
error: The name a has already been declared as an other argument.
*/
