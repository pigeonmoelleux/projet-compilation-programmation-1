package main

import "fmt"

func main() {
	fmt.Print(*nil)
}

/*
== Expected compiler output ==
File "./../tests/bad/testfile-nil-2.go", line 6, characters 11-15:
error: It is impossible to deference a nil pointer
*/
