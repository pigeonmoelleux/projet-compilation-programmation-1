package main

import "fmt"

type S struct {
	x int
	y string
	z *int
}

func main() {
	var t S
	a := 1
	t.x = 1
	t.y = "coucou"
	t.z = &a
	*t.z = 2
	fmt.Print(t.x, " ", t.y, " ", *t.z, " ", a, "\n")
	t.z = &t.x
	*t.z = 4
	fmt.Print(t.x, " ", *t.z, " ", a, "\n")
	t.x = 3
	fmt.Print(t.x, " ", *t.z, " ", a, "\n")
}

/*
== Expected program output ==
1 coucou 2 2
4 4 2
3 3 2
*/
