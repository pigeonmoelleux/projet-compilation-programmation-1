package main

import "fmt"

func main() {
	{
		a := 2
		fmt.Print(a)
	}
	{
		a := 3
		fmt.Print(a)
	}
}

/*
== Expected program output ==
23
*/
