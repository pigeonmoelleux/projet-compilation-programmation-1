package main

import "fmt"

func main() {
	var a = 1
	a = 2
	fmt.Print(a)
	a = 3
}

/*
== Expected program output ==
2
*/
