package main

import "fmt"

func bar(n int, m int) int {
	z := 2*n - m
	return z
}

func foo(n int, m int) (int, int) {
	a := n + bar(n, m)
	b := m - bar(m, n)
	return b, a
}

func main() {
	a := 10
	b, d := foo(a, 2*a)
	c := "bonjour"
	fmt.Print(a, " ", b, " ", c, " ", d, "\n")
}

/*
== Expected program output ==
10 -10 bonjour 10
*/
