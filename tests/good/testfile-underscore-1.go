package main

import "fmt"

func main() {
	_ = 1
	x, _ := 3, 1
	y, _ := 4, "test"
	_ = 3
	fmt.Print(x+y, "\n")
}

/*
== Expected program output ==
7
*/
