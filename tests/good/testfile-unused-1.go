package main

import "fmt"

func main() { a, _ := 1, 2; fmt.Print(a) }

/*
== Expected program output ==
1
*/
