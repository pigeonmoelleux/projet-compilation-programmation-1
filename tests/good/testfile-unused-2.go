package main

import "fmt"

type T struct{ x int }

func main() {
	var a, b = 1, new(T)
	a, b.x = 3, 1
	fmt.Print(a, "\n")
}

/*
== Expected program output ==
3
*/
