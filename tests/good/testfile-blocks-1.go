package main

import "fmt"

func main() {
	for n := 1; n < 5; n++ {
		i := n
		fmt.Print(n, " ", i, "\n")
	}
}

/*
== Expected program output ==
1 1
2 2
3 3
4 4
*/
