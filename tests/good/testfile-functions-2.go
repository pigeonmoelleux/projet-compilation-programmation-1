package main

import "fmt"

func toto() int {
	return 5
}

func foo(n int) (int, int) {
	return n, toto()
}

func bar(a int, b int) (int, int) {
	return a - b, a + b
}

func main() {
	var n int = 7
	var a, b int = bar(foo(n))
	var c string = "test"
	fmt.Print(a, " ", b, c, "\n")
}

/*
== Expected program output ==
2 12test
*/
