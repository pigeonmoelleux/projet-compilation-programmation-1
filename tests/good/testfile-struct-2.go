package main

import "fmt"

type Coor struct {
	abs int
	ord int
}

type S struct {
	x int
	y int
	z *Coor
}

func main() {
	var s S
	s.x = 3
	s.y = 4
	s.z = new(Coor)
	s.z.abs = 1
	s.z.ord = 2
	fmt.Print(s.x, " ", s.y, " ", s.z, " ", s.z.abs, " ", s.z.ord, "\n")
}

/*
== Expected program output ==
3 4 &{1 2} 1 2
*/
