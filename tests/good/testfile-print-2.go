package main

import "fmt"

func main() {
	{
		var a int
		a = 1
		fmt.Print(a, "\n")
		var b int = 2
		fmt.Print(b, "\n")
		var c, d = 3, 4
		fmt.Print(c, " ", d, "\n")
		var e, f int = 5, 6
		fmt.Print(e, " ", f, "\n")
		fmt.Print(a, b, c, d, e, f, "\n")
	}

	{
		var a bool
		a = true
		fmt.Print(a, "\n")
		var b bool = false
		fmt.Print(b, "\n")
		var c, d = true, false
		fmt.Print(c, " ", d, "\n")
		var e, f bool = false, true
		fmt.Print(e, " ", f, "\n")
		fmt.Print(a, b, c, d, e, f, "\n")
	}

	fmt.Print(1+2+3, "coucou", 3-2-1, 45/2, 45%2, "\n")
}

/*
== Expected program output ==
1
2
3 4
5 6
1 2 3 4 5 6
true
false
true false
false true
true false true false false true
6coucou0 22 1
*/
