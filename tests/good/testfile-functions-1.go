package main

import "fmt"

func foo(n int) int {
	return n * 2
}

func fact(n int) int {
	if n <= 1 {
		return 1
	}
	return n * fact(n-1)
}

func main() {
	n := 5
	a := foo(n)
	b := fact(a)
	fmt.Print(a, " ", b, "\n")
}

/*
== Expected program output ==
10 3628800
*/
