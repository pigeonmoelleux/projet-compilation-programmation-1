package main

import "fmt"

func foo() (int, int) {
	return 1, 2
}

func main() {
	fmt.Print(foo())
}

/*
== Expected program output ==
1 2
*/
