# Projet compilation - Programmation 1

Projet de programmation 1 de compilation de Petit Go en OCaml

## TODO

### Premier rendu

- [x] Comprendre ce qui est demandé
- [x] Pouvoir visualiser les AST (merci Vincent !).
- [x] Phase 1
- [x] Phase 2.a
- [x] Phase 2.b
- [x] Compléter la fonction expr_desc dans typing.ml
- [x] Phase 3.a
- [x] Phase 3.b
- [x] Faire le typing pour transformer un AST en TAST
- [x] Débuger avec plus de tests
- [x] Faire encore plus de tests

### Second rendu

- [x] Comprendre ce qui est demandé
- [x] Afficher les types int, string, bool et le nil
- [x] Assembler les opérations unaires et binaires sur les types int, string et bool
- [x] Implémenter les variables de type int, bool et string
- [x] Implémenter la portée des variables
- [x] Ajout des pointeurs
- [x] Ajout des fonctions sans retour
- [x] Ajout des fonctions avec une valeur de retour
- [x] Ajout des fonctions avec plusieurs valeurs de retour
- [ ] Ajout des structures

## Commandes

### Commandes classiques

- Pour compiler le projet, exécuter dans le répertoire src : `make`.
- Pour compiler un fichier .go, exécuter : `./pgoc -options fichier.go`. Les options disponibles nativement sont : `--debug`, `--parse-only`, `--type-only` et `-help/--help`.
- Le fichier pretty.ml de Vincent Laveychine rajoute une nouvelle option, `--no-pretty`, permmettant d'exécuter le code sans générer les fichiers .dot (voir la partie suivante).

### Commandes debuggage

- Pour débuger, utiliser l'option `--debug`, permettant de vérifier les conditions dans main.ml (bien penser à placer les variable `debug` à `true` dans les fichier concernés).
- Pour pouvoir visualiser l'AST, on peut utiliser l'option `--parse-only` qui fera s'arrêter avant le typing.
- La compilation du fichier .go génèrera deux fichier .dot si l'option `--no-pretty` n'est pas appelée. Ensuite, utiliser la commande : `dot mon_fichier_ast.dot -Tsvg -o mon_fichier_ast.svg`, permettant d'obtenir un fichier .svg visualisable directement par le navigateur (merci à Vincent pour ça). On verra également l'AST apparaître dans la console (en décommentant la ligne associée), mais le fichier svg généré est beaucoup plus simple à utiliser pour voir l'arbre.

### Commande tests automatiques

Le fichier `launch.sh` (réalisé avec l'aide de Vincent Lafeychine) dans le répertoire tests permet de tester automatiquement tous les tests avec le compilateur. Pour cela, on place en commentaire dans chaque fichier de test l'instruction :

- `== Expected compiler output ==` suivi du message d'erreur attendu si le test est censé échoué ;
- `== Expected TAST ==` suivi d'une description du TAST s'il est censé réussir.

La liste de tous les tests effectués se trouve à la fin de `launch.sh`. De plus, pour lancer ce fichier de tests, il faut se trouver dans le dossier parent, puis lancer la commande `bash tests/launch.sh`. Pour chaque fichier de test, le message "OK" sera affiché si le test réussit, et "KO" suivi de la différence entre le message attendu et le message obtenu sinon.
